const { prisma } = require('../src/generated/prisma-client')

async function main() {
  await prisma.createUser({
    name: 'Ben',
    password: 'password',
    messages: {
        create: {
            content: 'Some Message content'
        }
    }
  })
  await prisma.createUser({
    name: 'admin',
    password: 'admin',
    messages: {
        create: {
            content: 'Some Message content',
        }
    }
  })
}

main().catch(e => console.error(e))