import { GraphQLServer } from 'graphql-yoga';
import { prisma } from './generated/prisma-client';

const resolvers = {
    Query: {
        users: (parent, args, context) => {
            return context.prisma.users();
        },
        user: async (parent, {credentials}, context) => {
            const { name, password } = credentials;
            if (!name || !password) {
                return;
            }
            const users = await context.prisma.users({where: {name, password}});
            if (users.length){
                return users[0];
            }
        },
        feed: async (parent, { sort, textFilter, pagination }, context) => {
            const queryObject = {};
            const hasSort = sort?.field && sort?.order;
            if (textFilter) {
                Object.assign(queryObject, {where: {content_contains: textFilter}});
            }
            const messagesCount = await prisma
                .messagesConnection({...queryObject})
                .aggregate()
                .count();
                
            if (hasSort) {
                // basic prisma order query looks like this "fieldName_ASC"
                const orderQuery = sort.field + '_' + sort.order.toUpperCase();
                Object.assign(queryObject, { orderBy: orderQuery });
            }
            if (pagination?.limit) {
                Object.assign(queryObject, {first: pagination.limit});
            }
            if (pagination?.skip) {
                Object.assign(queryObject, {skip: pagination.skip})
            }
            const messages = await context.prisma.messages(queryObject);
            return { messages, messagesCount };
        },
    },
    Mutation: {
        createUser: (parent, {credentials}, context) => {
            const { name, password } = credentials;
            if (name && password) {
                return context.prisma.createUser({ name, password });
            }
        },
        createMessage: (parent, { authorId, content, repliedTo }, context) => {
            const data = {};
            if (repliedTo) {
                Object.assign(data, {repliedTo: {connect: {id: repliedTo}}})
            }
            Object.assign(data, {content, author: { connect: { id: authorId } }});
            return context.prisma.createMessage(data);
        },
        deleteMessage: async (parent, { id }, context) => {
            await context.prisma.deleteMessage({ id });
            return true;
        },
        updateMessageContent: (parent, { id, content }, context) => {
            return context.prisma.updateMessage({data: {content}, where: {id}});
        },
        setPostReaction: async (parent, { messageId, userId, isLike }, context) => {
            // messageReactions - returns array of matches
            const messageReactions = await context.prisma.messageReactions({where: {messageId, userId}}); 

            // I were trying to find better approach (triggers), 
            // but count likes and dislikes manually that's what I came up with
            let likesDiff = 0;
            let dislikesDiff = 0;
            if (!messageReactions.length) {
                await context.prisma.createMessageReaction({ 
                    isLike,
                    messageId,
                    userId
                });
                isLike ? likesDiff++: dislikesDiff++;
            }

            if (messageReactions.length) {
                const messageReaction = messageReactions[0];

                if (messageReaction.isLike === isLike) {
                    await context.prisma.deleteMessageReaction({id: messageReaction.id});
                    isLike ? likesDiff--: dislikesDiff--;
                }

                if (messageReaction.isLike !== isLike) {
                    await context.prisma.updateMessageReaction({data: {isLike}, where: {id: messageReaction.id}});
                    isLike ? (likesDiff++, dislikesDiff--): (likesDiff--, dislikesDiff++);
                }
            }
            const message = await prisma.message({ id: messageId });
            const likesCount = message.likesCount + likesDiff;
            const dislikesCount = message.dislikesCount + dislikesDiff;
            return context.prisma.updateMessage({data: {likesCount, dislikesCount}, where: { id: messageId }});
        }
    },
    Subscription: {
        messagesChange: {
            subscribe: (parent, args, context) => {
                return context.prisma
                    .$subscribe
                    .message({
                        mutation_in: ['CREATED', 'DELETED']
                    }).node()
            },
            resolve: () => true
        },
        messageUpdate: {
            subscribe: (parent, args, context) => {
                return context.prisma
                    .$subscribe
                    .message({
                        mutation_in: ['UPDATED']
                    }).node()
            },
            resolve: (payload) => payload
        }
    },
    User: {
        messages: ({ id }) => {
            return context.prisma.user({id}).messages();
        }
    },
    Message: {
        author: ({ id }, args, context) => {
          return context.prisma.message({ id }).author();
        },
        repliedTo: ({ id }, args, context) => {
            return context.prisma.message({ id }).repliedTo();
        },
    }
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    prisma,
  },
})

server.start((options) => console.log(`Server is running on port ${options.port}`))
