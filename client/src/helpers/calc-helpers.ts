export const countPages = (messagesCount: number): number[] => {
    const pages: number[] = [];
    const pagesCount: number = Math.ceil(messagesCount / 10);

    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i);
    }
    return pages;
}