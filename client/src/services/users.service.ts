// import { dataStorageProvider } from '../storage';
import { User } from '../types/User';
import { DocumentNode } from 'graphql';
import { gql } from '@apollo/client';
import { dataStorageProvider } from '../storage';

export class UserService {
    public async getUsers(): Promise<User[]> {
        const response = await dataStorageProvider
            .query<{users: User[]}>({query: this.getUsersQuery}); 
        return response.data!.users!;
    }

    private getUsersQuery: DocumentNode = gql`
        query User {
            users {
                id
                name
            }
        }
    `
}

export const userService = new UserService();
