import { User, UserCredentials } from '../types/User';
import { gql, DocumentNode } from '@apollo/client';
import { dataStorageProvider } from '../storage';
export class AuthenticationService {
    private static createUserQuery: DocumentNode = gql`
        mutation createUser($credentials: UserCredentials!) {
            createUser(credentials: $credentials) {
                id
                name
            }
        }
    `;

    private static getUserQuery: DocumentNode = gql`
        query User($credentials: UserCredentials!) {
            user(credentials: $credentials) {
                id
                name
            }
        }
    `

    public async createUser(userName: string, password: string): Promise<User> {
        const credentials: UserCredentials = { name: userName, password };
    
        const response = await dataStorageProvider
            .mutate<{createUser: User}>({mutation: AuthenticationService.createUserQuery, variables: {credentials}});
        
        return response.data!.createUser!;
    }

    public async login(userName: string, password: string): Promise<User | null> {
        const credentials: UserCredentials = { name: userName, password };
        
        const response = await dataStorageProvider
            .query<{user: User}>({query: AuthenticationService.getUserQuery, variables: {credentials}});
        return response.data?.user || null;
    }
}

export const authenticationService = new AuthenticationService();
