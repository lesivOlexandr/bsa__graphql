import { dataStorageProvider } from '../storage';
import { Message, Feed } from '../types/Message';
import { Observable } from '../types/Observable';
import { User } from '../types/User';
import { DocumentNode } from 'graphql';
import { gql } from '@apollo/client';
import { Sort, Pagination } from '../types/app-state';

export class MessageService extends Observable<Feed> {
    private feed: Feed = {messagesCount: 0, messages: []};

    private sort: Sort | null = null;
    private textFilter: string | null = null;
    private pagination: Pagination = { limit: 10, skip: 0 };

    protected async onFirstSubscription(): Promise<void> {
        this.getMessages();

        dataStorageProvider.subscribe<{messagesChange: Message}>({query: this.onMessagesChange})
            .subscribe(async() => {
                await this.getMessages();
            });

        dataStorageProvider.subscribe<{messageUpdate: Message}>({query: this.onUpdateMessage})
            .subscribe(data => {
                const updated: Message = data.data!.messageUpdate;
                const messages: Message[] = this.feed.messages
                    .map(oneMessage => oneMessage.id === updated.id ? updated: oneMessage);

                this.setFeed({...this.feed, messages});
            });
    }

    private async getMessages(): Promise<Feed> {
        const queryOptions = { 
            sort: this.sort,
            textFilter: this.textFilter,
            pagination: this.pagination
        };
        const response = await dataStorageProvider
            .query<{feed: Feed}>({query: this.getMessagesQuery, variables: queryOptions});
        const feed: Feed = response.data!.feed;
        this.setFeed(feed);
        return feed;
    }

    public setFeed(feed: Feed): void {
        this.feed = feed;
        this.notifyObservers(feed);
    }

    // Is not this violating single responsibility principle?
    public async saveMessage(text: string, user: User, repliedTo?: Message): Promise<void> {
        console.log(repliedTo, 'repl message')
        const data = { content: text, authorId: user.id, repliedTo: repliedTo?.id };
        dataStorageProvider.mutate({mutation: this.addMessage, variables: data});
    }

    public async updateMessage(id: number, message: Message): Promise<void> {
        dataStorageProvider.mutate({mutation: this.updateMessageQuery, variables: {id, content: message.content}});
    }

    public async deleteMessage(id: number): Promise<void> {
        dataStorageProvider.mutate({mutation: this.deleteMessageQuery, variables:{id}});
    }

    public async setReaction(messageId: number, userId: string, isLike: boolean): Promise<void> {
        const data = {messageId, userId, isLike};
        dataStorageProvider.mutate({ mutation: this.setPostReaction, variables: data });
    }

    public configureSort(sort: Sort): void {
        if (sort.field && sort.order) {
            this.sort = sort;
            this.getMessages();
        } 
        if (!(sort.field || sort.order) && this.sort) {
            this.sort = null;
            this.getMessages();
        }
    }

    public setTextFilter(text: string) {
        this.textFilter = text || null;
        this.pagination.skip = 0;
        this.getMessages();
    }

    public setPage(pageNum: number) {
        const skip: number = (pageNum - 1) * this.pagination.limit;
        if (skip !== this.pagination.skip) {
            this.pagination.skip = skip;
            this.getMessages();
        }
    }


    private updateMessageQuery: DocumentNode = gql`
        mutation updateMessageContent($id: Int!, $content: String!) {
            updateMessageContent(id: $id, content: $content) {
                id
            }
        }
    `;

    private deleteMessageQuery: DocumentNode = gql`
        mutation deleteMessage($id: Int!) {
            deleteMessage(id: $id)
        }
    `;

    private addMessage: DocumentNode = gql`
        mutation createMessage($content: String!, $authorId: ID!, $repliedTo: Int) {
            createMessage(content: $content, authorId: $authorId, repliedTo: $repliedTo) { id }
        }
    `

    private setPostReaction: DocumentNode = gql`
        mutation setPostReaction($messageId: Int!, $userId: ID!, $isLike: Boolean!) {
            setPostReaction(messageId: $messageId, userId: $userId, isLike: $isLike) {
                id
            }
        }
    `

    private onMessagesChange: DocumentNode = gql`
        subscription onMessagesChange {
            messagesChange
        }
    `;

    private onUpdateMessage: DocumentNode = gql`
        subscription onUpdateMessage {
            messageUpdate {
                id
                createdAt
                updatedAt
                likesCount
                dislikesCount
                content
                author {
                    id
                    name
                }
                repliedTo {
                    id
                }
            }
        }
    `;

    private getMessagesQuery: DocumentNode = gql`
        query($sort: Sort, $textFilter: String, $pagination: Pagination) {
            feed(sort: $sort, textFilter: $textFilter, pagination: $pagination) {
                messagesCount
                messages {
                    id
                    createdAt
                    updatedAt
                    likesCount
                    dislikesCount
                    content
                    author {
                        id
                        name
                    }
                    repliedTo {
                        id
                    }
                }
            }
        }
    `
}

export const messageService = new MessageService();
