import React from 'react';
import classNames from "classnames";
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import './index.css';
import { messageService } from '../../services/message.service';
import { setEditedMessage } from '../../reducers/actions';
import { connect } from 'react-redux';

interface Props {
    message: Message;
    user: User;
    setEditedMessage: (message: Message | null) => void;
}

const MessageComponent: React.FC<Props> = (props) => {
    const {message, user} = props;
    const isMessageOfCurrentUser: boolean = message.author.id === user.id;

    const onSetReaction = ({ isLike }: {isLike: boolean}) => {
        messageService.setReaction(message.id, user.id, isLike);
    }

    const onClickDelete = () => {
        messageService.deleteMessage(message.id);
    }

    const onClickEdit = () => {
        if (message.repliedTo) {
            message.repliedTo = null;
        }
        props.setEditedMessage(message);
    }

    const onClickReply = () => {
        if (message.repliedTo) {
            alert('You can\'t reply to this message');
            return;
        }
        const replyMessage: Message = new Message('', user, message);
        props.setEditedMessage(replyMessage);
    }

    return (
        <div className={classNames({
            'chat-body__chat-message': true,
            'chat-body__chat-message_user-current': isMessageOfCurrentUser
        })}>
            {!isMessageOfCurrentUser &&
                <div className="chat-message__user-avatar-wrapper">
                    <img className="chat-message__user-avatar" src="/user.png" alt="" />
                </div>
            }
            <div className="chat-message__text-wrapper">
                <p className="chat-message__message-header">
                    Message #{message.id}
                </p>
                <p className="chat-message__text">
                    { message.content }
                </p>
                <div className="chat-message__message-meta">
                    <div className="message-meta__message-icons">
                        <div className={classNames({
                            "message-icons__likes-block": true,
                            "message-meta__like-icon_owner-current-user": isMessageOfCurrentUser
                        })}>
                            <span className="likes-block__likes-count">
                                {message.likesCount}
                            </span>
                            <span
                                onClick={() => {onSetReaction({ isLike: true })}}
                                title="like"
                                className={classNames({
                                    "material-icons message-meta__like-icon": true,
                                    "message-icon": true
                            })}>
                                thumb_up
                            </span>
                        </div>
                            <div className={classNames({
                                "message-icons__likes-block": true,
                                "message-meta__like-icon_owner-current-user": isMessageOfCurrentUser
                            })}>
                                <span className="likes-block__likes-count">
                                    {message.dislikesCount}
                                </span>
                                <span 
                                    onClick={() => {onSetReaction({ isLike: false })}}
                                    title="dislike"
                                    className={classNames({
                                    "material-icons message-meta__like-icon": true,
                                    "message-icon": true
                                })}>
                                    thumb_down
                                </span>
                            </div>
                            <span 
                                className="material-icons message-icon"
                                title="reply"
                                onClick={onClickReply}
                            >
                                reply
                            </span>
                            {isMessageOfCurrentUser &&
                                <>
                                    <span 
                                        className="material-icons message-icon hidden-until"
                                        title="edit"
                                        onClick={onClickEdit}
                                    >
                                        settings
                                    </span>
                                    <span 
                                        className="material-icons message-icon hidden-until"
                                        title="delete"
                                        onClick={onClickDelete}
                                    >
                                        delete
                                    </span>
                                </>
                            }
                        </div>
                        <div className="message-meta__time-send">
                        { message?.repliedTo?.id && 'replied to message #' + message.repliedTo.id}
                    </div>
                </div>
            </div>
        </div>
    );
}

(MessageComponent as any).propTypes = {
  message: propTypes.object.isRequired,
  user: propTypes.object.isRequired
}

const mapDispatchToProps = {
  setEditedMessage
}

export default connect(null, mapDispatchToProps)(MessageComponent);
