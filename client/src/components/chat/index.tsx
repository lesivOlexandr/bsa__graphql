import React, { useEffect } from 'react';
import propTypes from 'prop-types';
import MessageList from '../message-list';
import ChatHeader from '../chat-header';
import MessageInput from '../message-input';
import EditMessageModal from '../edit-message-modal';
import { Message } from '../../types/Message';
import { AppState } from '../../types/app-state';
import { connect } from 'react-redux';
import { setEditedMessage } from '../../reducers/actions';
import { User } from '../../types/User';
import ChatOptions from '../chat-options';
import ChatFooter from '../chat-footer';


interface Props {
    messages: Message[],
    user: User,
    editedMessage: Message | null,
    setEditedMessage: (message: Message | null) => void
}

const ChatComponent: React.FC<Props> = (props) => {
    const {messages, user, editedMessage,} = props;
    const handleKeyPress = (e: KeyboardEvent) => {
        if (e.keyCode === 38 && !editedMessage) {
            e.preventDefault();
            const lastMessage = messages.reverse().find(message => message.author.id === user.id);
            if (lastMessage) {
                props.setEditedMessage(lastMessage);
            }
        }
    }

    useEffect(() => {
        document.addEventListener('keydown', handleKeyPress);
        return () => document.removeEventListener('keydown', handleKeyPress);
    })

    return (
        <div className="chat">
            <ChatHeader />
            <ChatOptions />
            <MessageList />
            <ChatFooter />
            <MessageInput />
            {editedMessage && <EditMessageModal />}
        </div>
    )
}

(ChatComponent as any).propTypes = {
    editedMessage: propTypes.object
}

const mapStateToProps = (state: AppState) => ({
    user: state.user!,
    messages: state.feed.messages,
    editedMessage: state.editedMessage
});

const mapDispatchToProps = {
    setEditedMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatComponent);
