import React from 'react';
import './index.css';
import { messageService } from '../../services/message.service';
import { AppState } from '../../types/app-state';
import { connect } from 'react-redux';
import { countPages } from '../../helpers/calc-helpers';

interface Props {
    messagesCount: number;
}

const ChatFooter: React.FC<Props> = (props) => {
    const pages: number[] = countPages(props.messagesCount);
    const onPageChange = (e: MouseEvent, pageNum: number) => {
        e.preventDefault();
        messageService.setPage(pageNum);
    };

    return (
        <div className="chat__chat-footer">
            <div className="chat-footer__pagination-container">
                <ol className="chat-footer__pagination">
                    {pages.map((page: number) => (
                        <li 
                            className="pagination__item"
                            key={page}
                        >
                            <a 
                                href="/" 
                                onClick={(e: any) => onPageChange(e, page)}
                            >
                                {page}
                            </a>
                        </li>
                    ))}
                </ol>
            </div>
        </div>
    )
}

const mapStateToProps = (state: AppState) => ({
    messagesCount: state.feed.messagesCount
});

export default connect(mapStateToProps)(ChatFooter);