import React, { useState, ChangeEvent } from 'react';
import { Sort } from '../../types/app-state';
import { orders, sortFields } from '../../types/enums';
import { messageService } from '../../services/message.service';

const ChatOptions = () => {
    const [sort, setSort] = useState<Sort>({ field: null, order: null });
    const [text, setFilterText] = useState<string>('');

    const onSortChange = (e: ChangeEvent<HTMLSelectElement>) => {
        const fieldName: string = e.target.name;
        const fieldValue: string = e.target.value;
        const updated = {...sort, [fieldName]: fieldValue}

        setSort(updated);
        messageService.configureSort(updated);
    }

    const onTextFilterChange = (e: ChangeEvent<HTMLInputElement>) => {
        const value: string = e.target.value;
        setFilterText(value);
        messageService.setTextFilter(value);
    }

    return (
        <div className="chat__options">
            <form>
                <select
                    value={sort.order || ''} 
                    onChange={onSortChange}
                    name="order"
                >
                    <option value=""></option>
                    <option value={orders.asc}>ASC</option>
                    <option value={orders.desc}>DESC</option>
                </select>
                <select 
                    value={sort.field || ''}
                    onChange={onSortChange}
                    name="field"
                >
                    <option value=""></option>
                    <option value={sortFields.createAt}>Sort by date of creation</option>
                    <option value={sortFields.likesCount}>Sort by likes count</option>
                    <option value={sortFields.dislikesCount}>Sort by dislikes count</option>
                </select>
                <input
                    value={text}
                    placeholder="message should include"
                    onChange={onTextFilterChange}/>
            </form>
        </div>
    );
}
export default ChatOptions;