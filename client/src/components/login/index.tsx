import React, { ChangeEvent } from 'react';
import propTypes from 'prop-types';
import { authenticationService } from '../../services/authentication.service';
import { User, UserCredentials } from '../../types/User';

import './index.css';

interface Props {
    onAuthorized: (user: User) => void;
}

const LoginComponent: React.FC<Props> = (props) => {
    const userData: UserCredentials = {name: '', password: ''};

    const onSignUp = async () => {
        const user: User = await authenticationService.createUser(userData.name, userData.password);
        props.onAuthorized(user);
    }

    const onLogin = async () => {
        const user: User | null = await authenticationService.login(userData.name, userData.password);
        if (user) {
            props.onAuthorized(user);
        } else {
            alert('No such user');
        }
    }

    const onChange = (event: ChangeEvent<HTMLInputElement>) => {
        const fieldName: string = event.target.name;
        const fieldValue: string = event.target.value;

        userData[fieldName as keyof UserCredentials] = fieldValue;
    }

    return (
        <div className="login-page">
            <form className="login-page__login-form">
                <input
                    className="login-form__item login-form__field"
                    name="name"
                    placeholder="Username"
                    onChange={onChange}
                />
                <input
                    className="login-form__item login-form__field"
                    name="password"
                    type="password"
                    placeholder="Password"
                    onChange={onChange}
                />
                <div className="submit-buttons-wrapper login-form__item">
                    <button className="login-form__submit-button" type="button" onClick={onLogin}>Login</button>
                    <button className="login-form__submit-button" type="button" onClick={onSignUp}>Create account</button>
                </div>
                <p>
                    Example credentials: 
                    Username: Ben, 
                    Password: password
                </p>
            </form>
        </div>
    );
}

(LoginComponent as any).propTypes = {
    onAuthorized: propTypes.func.isRequired
}

export default LoginComponent;
