import React from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import MessageComponent from '../message/message';
import { AppState } from '../../types/app-state';

import './index.css';

interface Props {
    messages: Message[],
    user: User
}

const MessageList: React.FC<Props> = (props) => {
    const { messages, user } = props;
    return (
        <div className="chat__chat-body"> 
            {messages.map(message => <MessageComponent user={user} message={message} key={message.id} />)}
        </div>
    );
}

(MessageList as any).propTypes = {
    messages: propTypes.arrayOf(propTypes.object).isRequired,
    user: propTypes.object.isRequired
}

const mapStateToProps = (state: AppState) => {
    return {
        messages: state.feed.messages,
        user: state.user!
    }
}

export default connect(mapStateToProps)(MessageList);