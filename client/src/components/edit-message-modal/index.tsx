import React, { FormEvent, ChangeEvent } from 'react';
import { setEditedMessage } from '../../reducers/actions';
import { connect } from 'react-redux';
import { Message } from '../../types/Message';
import { AppState } from '../../types/app-state';
import { messageService } from '../../services/message.service';

import './index.css';
import { User } from '../../types/User';

interface Props {
    editedMessage: Message;
    user: User;
    setEditedMessage: (data: Message | null) => void;
}

const EditMessageModal: React.FC<Props> = (props) => {
    const { editedMessage } = props;
    let inputtedEditText = '';

    const onCancel = (e: FormEvent) => {
        e.preventDefault();
        props.setEditedMessage(null);
    }

    const onSubmitEdit = (e: FormEvent) => {
        e.preventDefault();
        props.setEditedMessage(null);
        if (!inputtedEditText) {
            return;
        }
        editedMessage.content = inputtedEditText;
        if (!editedMessage.repliedTo) {
            messageService.updateMessage(editedMessage.id, editedMessage);
        }
        if (editedMessage.repliedTo) {
            messageService.saveMessage(editedMessage.content, props.user, editedMessage.repliedTo);
        }
    }

    const onTextChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
        inputtedEditText = e.target.value;
    }

    return (
        <div className="chat__modal-backdrop">
            <div className="chat__modal-container">
                <form className="modal-container-wrapper" onReset={onCancel} onSubmit={onSubmitEdit}>
                    <h3>
                        {editedMessage.repliedTo
                            ? 'Write reply to message #' + editedMessage.repliedTo.id
                            : 'Add new message text'
                        }</h3>
                    <textarea 
                        className="modal-container__edit"
                        placeholder="message" 
                        defaultValue={editedMessage.content}
                        onChange={onTextChange} 
                    />
                    <div className="modal-container__buttons">
                        <button className="modal-container__submit-edit-button" type="submit">Save</button>
                        <button className="modal-container__cancel-edit-button" type="reset">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

const mapStateToProps = (state: AppState) => ({
    editedMessage: state.editedMessage!,
    user: state.user!
});

const mapDispatchToProps = {
    setEditedMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
