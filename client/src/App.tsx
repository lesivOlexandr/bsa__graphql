import React, { useState } from 'react';
import { connect } from 'react-redux';
import ChatComponent from './components/chat'; 
import LoginComponent from './components/login';
import AppHeader from './components/app-header';
import AppFooter from './components/app-footer/';
import Preloader from './components/preloader';
import { User } from './types/User';
import { Feed } from './types/Message';
import { messageService } from './services/message.service';
import { userService } from './services/users.service';
import { setUsers, setCurrentUser, setFeed } from './reducers/actions';
import { AppState } from './types/app-state';

import './App.css';

interface Props {
    user: User | null;
    setUsers: (users: User[]) => void;
    setCurrentUser: (user: User) => void;
    setFeed: (feed: Feed) => void;
}

const App: React.FC<Props> = (props) => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const { user } = props;

    const onAuthorized = (user: User) => {
        props.setCurrentUser(user);
        setIsLoading(true);
        messageService.subscribe((feed: Feed) => {props.setFeed(feed); setIsLoading(false);});
        userService.getUsers().then((users: User[]) => props.setUsers(users));
    };

    const getContent = () => {
        return isLoading ? <Preloader /> : <ChatComponent />;
    };

    return (
        <div className="app">
        <AppHeader />
            {user 
                ? getContent()
                : <LoginComponent onAuthorized={onAuthorized} />
            }
        <AppFooter />
        </div>
    );
}

const mapStateToProps = (state: AppState) => {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = {
    setUsers, setCurrentUser, setFeed
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
