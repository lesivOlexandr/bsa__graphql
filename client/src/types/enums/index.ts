export enum orders {
    desc = 'desc',
    asc = 'asc'
}

export enum sortFields {
    createAt = 'createdAt',
    likesCount = 'likesCount',
    dislikesCount = 'dislikesCount'
}
