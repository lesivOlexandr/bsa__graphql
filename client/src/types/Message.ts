import { User } from "./User";

export class Message {
    constructor(
        public content: string,
        public author: User,
        public repliedTo: Message | null = null,
        public likesCount: number = 0,
        public dislikesCount: number = 1,
        public createdAt: number = Date.now(),
        public updatedAt: number | null = null,
        public id: number = null!,
    ){}
}


export interface Feed {
    messagesCount: number;
    messages: Message[];
}
