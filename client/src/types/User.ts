export interface UserCredentials {
    name: string,
    password: string
}

export class User {
    constructor(
        public id: string,
        public name: string,
    ){}
}
