import { User } from "./User";
import { Message, Feed } from "./Message";
import { orders, sortFields } from "./enums";

export class AppState {
    constructor(
        public users: User[] = [],
        public user: User | null = null,
        public feed: Feed = {messagesCount: 0, messages: []},
        public editedMessage: Message | null = null 
    ){}
}

export interface Sort {
    order: orders | null,
    field: sortFields | null
}

export interface Pagination {
    skip: number,
    limit: number
}
