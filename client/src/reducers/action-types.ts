export enum actionTypes {
    setUsers = 'SET_USERS',
    setCurrentUser = 'SET_CURRENT_USER',
    setFeed = 'SET_FEED',
    setEditedMessage = 'SET_EDITED_MESSAGE'
}
